![thesaurus_graphic.png](https://bitbucket.org/searleb/thesaurus/downloads/thesaurus_graphic.png)

The human phosphorylation regulatory network represents a complex signaling cascade where proteins can be phosphorylated at multiple sites resulting in different functional states. Here we present Thesaurus, a hybrid search engine that detects, localizes, and quantifies novel positional isomers using site-specific fragment ions directly from data independent acquisition mass spectrometry experiments. This software works for several PTMs in addition to phosphorylation. Check out our paper describing Thesaurus and what it can do at Nature Methods [(Searle et al, 2019)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7012383/)!

### How do I get Thesaurus? ###
The Thesaurus project has been merged into [EncyclopeDIA](https://bitbucket.org/searleb/encyclopedia), a joint open source project with the same goals. You can now access the latest version of Thesaurus in [encyclopedia-0.9.5](https://bitbucket.org/searleb/encyclopedia/downloads/encyclopedia-0.9.5-executable.jar) or higher. 

Thesaurus is open source under the [Apache 2 licence](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)), which means you can do what you like with the software, as long as you include the required notices. We recommend that you download the [human phosphoproteome library](https://phosphopedia.gs.washington.edu/PhosphoproteomicsAssay/sections/elib.xhtml) originally published as [Lawrence et al, 2016](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5915315/).

|Stable versions:|Release Date|Changes|
|------------------|---------------|------------------------------|
|[encyclopedia-0.9.5](https://bitbucket.org/searleb/encyclopedia/downloads/encyclopedia-0.9.5-executable.jar)|2020-06-05|Added Thesaurus into the main EncyclopeDIA build|
|[thesaurus-0.9.4](https://bitbucket.org/searleb/thesaurus/downloads/thesaurus-0.9.4-executable.jar)|2020-03-10|new flags to search for positional isomers and additional PTMs|
|[thesaurus-0.6.4](https://bitbucket.org/searleb/thesaurus/downloads/thesaurus-0.6.4-executable.jar)|2018-03-22|improved sensitivity, localization FDR|
|[thesaurus-0.5.7](https://bitbucket.org/searleb/thesaurus/downloads/thesaurus-0.5.7-executable.jar)|2017-09-17|original upload|

### How do I use Thesaurus? ###
Thesaurus is a cross-platform Java application that has been tested for Windows, Macintosh, and Linux. Specifically, we have tested it under Windows 8 and 10, Mac OS X 10.11-13, and RedHat Linux 4.4. The Thesaurus GUI can be opened by double clicking on the Thesaurus .JAR (e.g. thesaurus-0.5.4-executable.jar). Alternatively, Thesaurus can be used through the command line. Thesaurus requires 64-bit Java 1.8. If you don't already have it, you can download either the "Windows x64 Offline", "Mac OS X x64 .DMG" or "Linux x64" link from the [Java SE Runtime Environment 8 downloads website](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html), depending on your specific operating system. The download and installation time should take only a few minutes.

We recommend that you download the manual. The manual includes a quickstart walkthrough to help you analyze a simple PRM example using a [demo library](https://bitbucket.org/searleb/thesaurus/downloads/20160718_FU_bcs_4b_PRM.mzML.dlib) and [demo data](https://bitbucket.org/searleb/thesaurus/downloads/20160718_FU_bcs_4b_PRM.mzML.zip). The manual includes instructions on how to process your own data using the GUI. While the GUI is designed to enable fast and easy analysis for most experiments, the manual describes how to use the command-line options to access additional features. Thesaurus ELIB reports are produced as SQLite Documents. The manual includes the tabular structure of these documents, as well as a list of common queries. We recommend using the [SQLite Browser](https://sqlitebrowser.org/) to graphically browse the reports.

### Who do I talk to? ###
This is a Villen Lab and MacCoss Lab project from the University of Washington, Department of Genome Sciences. It is now maintained by the Searle lab at the Ohio State University Medical Center. For more information please contact Brian Searle (brian dot searle at osumc dot edu).

### Contribution guidelines ###
Code contributions are very welcome! However, any contribution must follow the coding style of the project, be presented with tests and stand up to code review before it will be accepted. We recommend that you contact us directly before submitting pull requests on our approved practices.